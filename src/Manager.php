<?php
namespace Plinker\Peer {

    use RedBeanPHP\R;
    use Opis\Closure\SerializableClosure;

    class Manager {

        public $config = array();
        private $tab;

        public function __construct(array $config = array())
        {
            $this->config = $config;
            
            // load models
            $this->model = new Model($this->config['database']);
        }
        
        /**
         *
         */
        public function config(array $params = array())
        {
            if (!empty($params[0])) {
                
                $params[0]['plinker']['peer'] = !empty($params[0]['plinker']['peer']) ? $params[0]['plinker']['peer'] : $this->config['plinker']['peer'];
                $params[0]['plinker']['tracker'] = !empty($params[0]['plinker']['tracker']) ? $params[0]['plinker']['tracker'] : $this->config['plinker']['tracker'];
                $params[0]['plinker']['public_key'] = !empty($params[0]['plinker']['public_key']) ? $params[0]['plinker']['public_key'] : $this->config['plinker']['public_key'];
                $params[0]['plinker']['private_key'] = !empty($params[0]['plinker']['private_key']) ? $params[0]['plinker']['private_key'] : $this->config['plinker']['private_key'];
                $params[0]['plinker']['enabled'] = !empty($params[0]['plinker']['enabled']) ? true : $this->config['plinker']['enabled'];
                $params[0]['plinker']['encrypted'] = !empty($params[0]['plinker']['encrypted']) ? true : $this->config['plinker']['encrypted'];
                $params[0]['database']['dsn'] = !empty($params[0]['database']['dsn']) ? $params[0]['database']['dsn'] : $this->config['database']['dsn'];
                $params[0]['database']['username'] = !empty($params[0]['database']['username']) ? $params[0]['database']['username'] : $this->config['database']['username'];
                $params[0]['database']['password'] = !empty($params[0]['database']['password']) ? $params[0]['database']['password'] : $this->config['database']['password'];
                $params[0]['database']['freeze'] = !empty($params[0]['database']['freeze']) ? true : $this->config['database']['freeze'];
                $params[0]['database']['debug'] = !empty($params[0]['database']['debug']) ? true : $this->config['database']['debug'];
                $params[0]['debug'] = !empty($params[0]['debug']) ? $params[0]['debug'] : $this->config['debug'];
                $params[0]['sleep_time'] = !empty($params[0]['sleep_time']) ? (int) $params[0]['sleep_time'] : $this->config['sleep_time'];
                $params[0]['webui']['user'] = !empty($params[0]['webui']['user']) ? $params[0]['webui']['user'] : $this->config['webui']['user'];
                $params[0]['webui']['pass'] = !empty($params[0]['webui']['pass']) ? $params[0]['webui']['pass'] : $this->config['webui']['pass'];

                file_put_contents('../config.php', "<?php
/**
 * Config - Defines stuff!..
 */

\$config = [
    // plinker configuration
    'plinker' => [
        // self should point to this instance
        'peer' => '{$params[0]['plinker']['peer']}',

        // tracker which seeds peers
        'tracker' => '{$params[0]['plinker']['tracker']}',

        // network keys
        'public_key'  => '{$params[0]['plinker']['public_key']}',
        // required to add nodes
        'private_key' => '{$params[0]['plinker']['private_key']}',

        'enabled' => ".(!empty($params[0]['plinker']['enabled']) ? 'true' : 'false').",
        'encrypted' => ".(!empty($params[0]['plinker']['encrypted']) ? 'true' : 'false')."
    ],

    // database connection
    // default: sqlite:'.__DIR__.'/database.db
    'database' => [
        'dsn' => '{$params[0]['database']['dsn']}',
        'username' => '{$params[0]['database']['username']}',
        'password' => '{$params[0]['database']['password']}',
        'freeze' => ".(!empty($params[0]['database']['freeze']) ? 'true' : 'false').",
        'debug' => ".(!empty($params[0]['database']['debug']) ? 'true' : 'false')."
    ],

    // displays output to consoles
    'debug' => true,

    // daemon sleep time
    'sleep_time' => {$params[0]['sleep_time']},

    // webui login
    'webui' => [
        'user' => '{$params[0]['webui']['user']}',
        'pass' => '{$params[0]['webui']['pass']}'
    ]
];

//
date_default_timezone_set('Europe/London');

// define debug error reporting/output
if (\$config['debug'] === true) {
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
}");
            }
            
            return $this->config['config'];
        }
        
        // /**
        //  * 
        //  */
        // public function hi(array $params = array())
        // {
        //     $this->model->store($this->model->create(['log', 'time' => time(), 'action' => 'hi', 'ip' => $this->getIPAddress()]));
            
        //     $peer = @$params[0];

        //     if (!empty($peer['token'])) {
        //         // find peer
        //         $peer = $this->model->findOne('peer', 'token = ?', [
        //             $peer['token']
        //         ]);
                
        //         if (empty($peer)) {
        //             return new \Exception('Unauthorized');
        //         }
                
        //         return 'Hello im: '.$this->getIPAddress();
        //     } else {
        //         return new \Exception('Peer announce record empty.');
        //     }
        // }
        

        // /**
        //  * 
        //  */
        // public function announce(array $params = array())
        // {
        //     if (empty($params[0])) {
        //         return new \Exception('Peer callback url is required.');
        //     }
            
        //     $this->model->store($this->model->create(['log', 'time' => time(), 'action' => 'announce', 'ip' => $this->getIPAddress()]));
            
            
        //     $peer = $this->model->findOrCreate([
        //         'peer',
        //         'ip' => $this->getIPAddress(),
        //         'peer' => $params[0]
        //     ]);

        //     $peer->token = hash('sha256', (
        //         $_SERVER['REMOTE_ADDR'].$params[0].microtime(true)
        //     ));
            
        //     $this->model->store($peer);
            
        //     return $peer;
        // }
        
        // public function peers(array $params = array())
        // {
        //     $this->model->store($this->model->create(['log', 'time' => time(), 'action' => 'peers', 'ip' => $this->getIPAddress()]));
            
        //     $peer = @$params[0];

        //     if (!empty($peer['token'])) {
        //         // find peer
        //         $peer = $this->model->findOne('peer', 'token = ?', [
        //             $peer['token']
        //         ]);
                
        //         if (empty($peer)) {
        //             return new \Exception('Unauthorized');
        //         }
                
        //         return $this->model->findAll('peer');
        //     } else {
        //         return new \Exception('Peer announce record empty.');
        //     }
        // }
        
        // public function broadcast(array $params = array())
        // {
        //     $this->model->store($this->model->create(['log', 'time' => time(), 'action' => 'broadcast', 'ip' => $this->getIPAddress()]));
            
        //     $peer = @$params[0];

        //     if (!empty($peer['token'])) {
                
        //         // find peer
        //         $peer = $this->model->findOne('peer', 'token = ?', [
        //             $peer['token']
        //         ]);
                
        //         if (empty($peer)) {
        //             return new \Exception('Unauthorized');
        //         }
                
        //         $peers =  $this->model->findAll('peer');
                
        //         foreach ($peers as $id => $own_peer) {
        //             $peer_network = new \Plinker\Core\Client(
        //                 $own_peer->peer,
        //                 'Peer\Peer',
        //                 hash('sha256', gmdate('h').$this->config['plinker']['public_key']),
        //                 hash('sha256', gmdate('h').$this->config['plinker']['private_key']),
        //                 $this->config,
        //                 $this->config['plinker']['encrypted']
        //             );
                    
        //             $result[$id] = $peer_network->announce($peer['peer']);
                    
        //             $result[$id]['response'] = $peer_network->{@$params[1]}($result[$id]);
        //         }

        //     } else {
        //         return new \Exception('Peer announce record empty.');
        //     }

        //     return $result;
        // }

        // public function disconnect(array $params = array())
        // {
        //     $peer =  $this->model->findOrCreate([
        //         'peer',
        //         'ip' => $this->getIPAddress()
        //     ]);
            
        //     $this->model->trash($peer);
            
        //     return true;
        // }
        
        // public function testClosure($params = array())
        // {
        //     $test = function ($what) {
        //         return $what.' - Thanks buddy...';
        //     };
    
        //     return new SerializableClosure($test);
        // }

    }

}
